"""Module for initializing simulation snapshots"""
from . import ff
from hoomd import deprecated
from hoomd import md


def remove_hidden_types(types):
    return [_ for _ in types if not _.startswith("_")]

def tabulated_dihedral(system, factor=1.0, model_file="model.xml", model_name="default"):
    """Creates all the LJ, bond, angle, and dihedral constraints for a hoomd simulation.
    Args:
        system (hoomd.system): Must be initialized before passed in.
        factor (float): Can be used to scale all epsilons.
        model_file (str): Name of file to look for models in.
        model_name (str): Name of model to look for in model file.
    """
    particle_types = system.particles.types
    atom_types = list(set(system.particles.types))
    bond_types = list(set([i.type for i in system.bonds]))
    angle_types = list(set([i.type for i in system.angles]))
    dihedral_types = list(set([i.type for i in system.dihedrals]))

    my_ff = ff.hoomd_ff(model_file, model_name, tabulated_dihedral=True)

    p = my_ff.generate_pairs(atom_types)
    m,s,e = my_ff.read_atom_params(atom_types)
    b,a,d = my_ff.read_constraint_params(bond_types,angle_types,dihedral_types, tabulated_dihedral=True)

    lj=pair.lj(r_cut=2.5)
    lj.set_params(mode="xplor")

    for key,val in p.iteritems():
        lj.pair_coeff.set(key[0],key[1],epsilon=factor*val[0],sigma=val[1])
    if len(bond_types)>0:
        bonds = bond.harmonic()
        for key, val in b.iteritems():
            bonds.bond_coeff.set(key,k=val[1],r0=val[0])
    else:
        bonds= None
    if len(angle_types)>0:
        angles = angle.harmonic()
        for key,val, in a.iteritems():
            angles.set_coeff(key,k=val[1],t0=val[0])
    else:
        angles = None

    def multi_harmonic(theta, k0, k1, k2, k3, k4):
        V = k0
        V+= k1*np.cos(theta)
        V+= k2*np.cos(theta)**2
        V+= k3*np.cos(theta)**3
        V+= k4*np.cos(theta)**4
        F = 1*k1*np.sin(theta)
        F+= 2*k2*np.sin(theta)*np.cos(theta)
        F+= 3*k3*np.sin(theta)*np.cos(theta)**2
        F+= 4*k4*np.sin(theta)*np.cos(theta)**3
        return (V,F)

    if len(dihedral_types)>0:
        dihedrals = dihedral.table(width=1024)
        for key, val in d.iteritems():
            dihedrals.dihedral_coeff.set(key, func = multi_harmonic, coeff=dict(k0=val[0]
                    , k1=val[1]
                    , k2=val[2]
                    , k3=val[3]
                    , k4=val[4]))
    else:
        angles = None
    return lj, bonds, angles, dihedrals

def interactions_from_xml_tabulated(system, factor=1.0, model_file="default_model.xml", model_name="default"):
    """Creates all the LJ, bond, angle, and dihedral constraints for a hoomd simulation.
    Args:
        system (hoomd.system): Must be initialized before passed in.
        factor (float): Can be used to scale all epsilons.
        model_file (str): Name of file to look for models in.
        model_name (str): Name of model to look for in model file.
    """
    # TODO fix this function for hoomd2
    particle_types = system.particles.types
    atom_types = list(set(system.particles.types))
    bond_types = list(set([i.type for i in system.bonds]))
    angle_types = list(set([i.type for i in system.angles]))
    dihedral_types = list(set([i.type for i in system.dihedrals]))
    my_ff = ff.hoomd_ff(model_file, model_name)

    p = my_ff.generate_pairs(atom_types)
    m,s,e = my_ff.read_atom_params(atom_types)
    b,a,d = my_ff.read_constraint_params(bond_types,angle_types,dihedral_types)

    def ljt (r, rmin, rmax, eps, sig):
        V = 4*eps*( (sig/r)**12 - (sig/r)**6)
        F = 4*eps*( 12*(sig/r)**12 - (sig/r)**6)/r
        return (V,F)

    lj=pair.table(width=1024)
    for key,val in p.items():
        lj.pair_coeff.set(key[0],key[1],func=ljt,rmin=0.7,rmax=3.0,coeff=dict(eps=factor*val[0],sig=val[1]))
    if len(bond_types)>0:
        bonds = bond.harmonic()
        for key, val in b.items():
            bonds.bond_coeff.set(key,k=val[1],r0=val[0])
    else:
        bonds= None
    if len(angle_types)>0:
        angles = angle.harmonic()
        for key,val, in a.items():
            angles.set_coeff(key,k=val[1],t0=val[0])
    else:
        angles = None
    if len(dihedral_types)>0:
        dihedrals = dihedral.opls()
        for key, val in d.items():
            dihedrals.set_coeff(key,k1=val[0],k2=val[1],k3=val[2],k4=val[3])
    else:
        angles = None
    return lj, bonds, angles, dihedrals


def interactions_from_xml_verbatim(system, factor=1.0, model_file="default_model.xml", model_name="default"):
    """Creates all the LJ, bond, angle, and dihedral constraints for a hoomd simulation.
    Does not do any scaling of the parameters passed in: just takes them verbatim.
    Args:
        system (hoomd.system): Must be initialized before passed in.
        factor (float): Can be used to scale all epsilons.
        model_file (str): Name of file to look for models in.
        model_name (str): Name of model to look for in model file.
    """
    particle_types = system.particles.types
    # Filter out _ types
    particle_types = remove_hidden_types(particle_types)
    atom_types = list(set(particle_types))
    bond_types = list(set([i.type for i in system.bonds]))
    angle_types = list(set([i.type for i in system.angles]))
    dihedral_types = list(set([i.type for i in system.dihedrals]))
    my_ff = ff.hoomd_ff(model_file, model_name)

    p = my_ff.generate_pairs(atom_types)
    m, s, e = my_ff.read_atom_params(atom_types)
    b, a, d = my_ff.read_constraint_params(bond_types, angle_types, dihedral_types)
    nl_c = md.nlist.cell()

    lj = md.pair.lj(r_cut=2.5, nlist=nl_c)
    lj.set_params(mode="xplor")
    for key, val in p.items():
        lj.pair_coeff.set(key[0], key[1], epsilon=factor*val[0], sigma=val[1])
    if len(bond_types) > 0:
        bonds = md.bond.harmonic()
        for key, val in b.items():
            bonds.bond_coeff.set(key, k=val[1], r0=val[0])
    else:
        bonds = None
    if len(angle_types) > 0:
        angles = md.angle.harmonic()
        for key, val, in a.items():
            angles.angle_coeff.set(key, k=val[1], t0=val[0])
    else:
        angles = None
    if len(dihedral_types) > 0:
        dihedrals = md.dihedral.opls()
        for key, val in d.items():
            dihedrals.dihedral_coeff.set(key, k1=val[0], k2=val[1], k3=val[2], k4=val[3])
    else:
        dihedrals = None
    return lj, bonds, angles, dihedrals, nl_c


def interactions_from_xml(system, factor=1.0, model_file="default_model.xml", model_name="default"):
    """Creates all the LJ, bond, angle, and dihedral constraints for a hoomd simulation.
    Args:
        system (hoomd.system): Must be initialized before passed in.
        factor (float): Can be used to scale all epsilons.
        model_file (str): Name of file to look for models in.
        model_name (str): Name of model to look for in model file.
    """
    # TODO fix this function for hoomd2
    particle_types = system.particles.types
    atom_types = list(set(system.particles.types))
    bond_types = list(set([i.type for i in system.bonds]))
    angle_types = list(set([i.type for i in system.angles]))
    dihedral_types = list(set([i.type for i in system.dihedrals]))
    my_ff = ff.hoomd_ff(model_file, model_name)
    my_ff.generate_LJ(atom_types)
    my_ff.generate_bonds(bond_types)
    my_ff.generate_angles(angle_types,units="radians")
    my_ff.generate_dihedrals(dihedral_types)
    #TODO: Add impropers?
    lj=pair.lj(r_cut=2.5)
    lj.set_params(mode="xplor")
    for key,val in my_ff.pairs.items():
        lj.pair_coeff.set(key[0],key[1],epsilon=factor*val[0],sigma=val[1])
    bonds = bond.harmonic()
    for key, val in my_ff.bonds.items():
            bonds.bond_coeff.set(key,k=val[1],r0=val[0])
    bonds.bond_coeff.set('vis',k=0.,r0=1.0)
    if len(angle_types)>0:
        angles = angle.harmonic()
        for key,val, in my_ff.angles.items():
            angles.set_coeff(key,k=val[1],t0=val[0])
    else:
        angles = None
    if len(dihedral_types)>0:
        dihedrals = dihedral.opls()
        for key, val in my_ff.dihedrals.items():
            dihedrals.set_coeff(key,k1=val[0],k2=val[1],k3=val[2],k4=val[3])
    else:
        angles = None
    return lj, bonds, angles, dihedrals

def make_blob(infile,model_file, model_name, T=3.0,outfile="blob.xml",factor=0.7,calc_charge=True):
    """Runs a short hoomd simulation to blob-ify an infile.

    Note that this must be invoked with 'hoomd ____.py' rather than 'python ____.py'
    """
    # TODO fix this function for hoomd2
    system = deprecated.init.read_xml(infile)
    lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=factor, model_file=model_file, model_name=model_name)
    integrate.mode_standard(dt=0.0001)
    rigid = group.rigid()
    nonrigid = group.nonrigid()
    print("T=",T)
    if len(rigid) > 0:
        integrate.bdnvt_rigid(group=rigid,T=T)
    if len(nonrigid) >0:
        integrate.bdnvt(group=nonrigid,T=T,limit=0.02)
    #nlist.reset_exclusions(exclusions=['bond','body'])
    nlist.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
    nn=32
    if calc_charge:
        pppm = charge.pppm(group=group.charged())
        pppm.set_params(Nx=nn,Ny=nn,Nz=nn,order=3,rcut=2.0)
    dump.dcd(filename="blob.dcd", overwrite=True, period=1e4)
    run(5e5)
    dump.xml(filename=outfile,vis=True, image=True, velocity=True)

#TODO(Eric): Add box randomizer.
