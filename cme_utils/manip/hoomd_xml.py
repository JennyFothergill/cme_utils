"""Module for reading and writing hoomd_xml files."""

import xml.etree.ElementTree as ET
import numpy
main_particle_properties = [ 'type',
                             'mass',
                             'diameter',
                             'charge',
                             'body',
                             'molecule',
                             'residue',
                             'backbone',
                             'sidechain']

spatial_particle_properties = [ 'position']#,
#                                'image',
#                                'velocity',
#                                'acceleration',
#                                'orientation',
#                                'moment_inertia']
constraints = ['bond',
               'angle',
               'dihedral',
               'improper']

system_properties = ['box',
                     'wall']

class hoomd_xml():
    """Container for reading and writing HOOMD XML data.
    
    Attributes:
        config_elements (list): List of properties that belongs to a molecule or snapshot configuration.
        root (Element): Top-level XML node of the element tree.
        config (Element): Configuration node of the element tree, holds most of the snapshot data.
    """
    def __init__(self,infile=None):
        """Initializes empty fields for each of the config_elements and a 100x100x100 box."""
        self.config_elements = main_particle_properties+spatial_particle_properties+system_properties+constraints
        self.root = ET.Element("hoomd_xml",version="1.5")
        self.root.text='\n'
        self.config = ET.Element("configuration",time_step="0",dimensions="3",natoms="-1")
        self.config.text='\n'
        self.config.tail='\n'
        for c in self.config_elements:
            ET.SubElement(self.config,c)
            self.config[-1].tail='\n'
            self.config[-1].text='\n'
        self.config.find('box').attrib['lx']='40'
        self.config.find('box').attrib['ly']='40'
        self.config.find('box').attrib['lz']='40'
        self.config.find('box').attrib['xy']='0'
        self.config.find('box').attrib['xz']='0'
        self.config.find('box').attrib['yz']='0'
        self.config.find('box').text=''

        if infile is not None:
            if infile[-3:]=="xml":
                self.read(infile)
            if infile[-3:]=="cml":
                self.from_cml(infile)

    def read(self,filename):
        """Initializes the XML tree from an existing XML file."""
        self.root = ET.parse(filename).getroot()
        self.config = self.root.find('configuration')
        self.root = ET.Element("hoomd_xml",version="1.5") #Necessary, since write() adds config to root
        self.root.text='\n'

    def write(self,filename,elements_to_write=['position']+main_particle_properties+system_properties+constraints):
        """Writes an XML file.

        Args:
            filename (str): Name of XML file to be written.
            elements_to_write (list): Optional list of elements that you wish to be written.

        Returns:
            None
        """
        for e in self.config_elements:
            if e not in elements_to_write:
                if self.config.find(e) is not None:
                    self.config.remove(self.config.find(e))
        self.root.insert(0,self.config)
        tree = ET.ElementTree(self.root)
        tree.write(filename, xml_declaration=True, encoding="UTF-8")
        
    def from_cml(self, filename):
        """Initializes the XML tree from an Avogadro CML file."""
        root = ET.parse(filename).getroot()
        atom_array = root.find('atomArray')
        N = str(len(atom_array))
        for e in main_particle_properties+spatial_particle_properties:
            self.config.find(e).attrib['num']=N
        self.config.attrib['natoms']=N
        for a in atom_array:
            self.config.find('position').text+= "{0} {1} {2}\n".format(a.attrib['x3'],a.attrib['y3'],a.attrib['z3'])
            self.config.find('type').text+= "{0}\n".format(a.attrib['elementType'])
            self.config.find('mass').text+= "1.0\n"
            self.config.find('diameter').text+= "1.0\n"
            self.config.find('charge').text+= "0.0\n"
            self.config.find('body').text+= "-1\n"
            self.config.find('molecule').text+= "-1\n"
            self.config.find('residue').text+= "-1\n"
            self.config.find('backbone').text+= "-1\n"
            self.config.find('sidechain').text+= "-1\n"
        bond_array=root.find('bondArray')
        self.config.find('bond').attrib['num']=str(len(bond_array))
        for b in bond_array:
            bond_type = "B"+b.attrib['order']
            a0,a1 = b.attrib['atomRefs2'].replace('a','').split()
            a0 = str(int(a0)-1)
            a1 = str(int(a1)-1)
            self.config.find('bond').text+= "{0} {1} {2}\n".format(bond_type,a0,a1)

    def from_building_block(self, bb):
        """Initializes the XML tree from an existing building block."""
        bb.set_defaults()
        N = str(bb.get_N_atoms())
        self.config.attrib['natoms']=N
        for e in main_particle_properties+spatial_particle_properties:
            self.config.find(e).attrib['num']=N
            print("There are,", len(bb.props[e]), e)
        for i in range(bb.N):
            self.config.find('position').text+= "{0} {1} {2}\n".format(bb.props['position'][i][0],bb.props['position'][i][1],bb.props['position'][i][2])
            for prop in main_particle_properties:
                if self.config.find(prop) is not None:
                    self.config.find(prop).text+= "{0}\n".format(bb.props[prop][i]) 
        self.config.find('bond').attrib['num']=str(len(bb.props['bond']))
        for b in bb.props['bond']:
            self.config.find('bond').text+="{0} {1} {2}\n".format(b[0],b[1],b[2])
        self.config.find('angle').attrib['num']=str(len(bb.props['angle']))
        for b in bb.props['angle']:
            self.config.find('angle').text+="{0} {1} {2} {3}\n".format(b[0],b[1],b[2],b[3])
        self.config.find('dihedral').attrib['num']=str(len(bb.props['dihedral']))
        for b in bb.props['dihedral']:
            self.config.find('dihedral').text+="{0} {1} {2} {3} {4}\n".format(b[0],b[1],b[2],b[3],b[4])
        self.config.find('improper').attrib['num']=str(len(bb.props['improper']))
        for b in bb.props['improper']:
            self.config.find('improper').text+="{0} {1} {2} {3} {4}\n".format(b[0],b[1],b[2],b[3],b[4])

    def get_xyz_box(self):
        N = int(self.config.find('position').attrib['num'])
        xyz = numpy.zeros(shape=(N,3))
        t = self.config.find('position').text.split()
        lx = float(self.config.find('box').attrib['lx'])
        ly = float(self.config.find('box').attrib['ly'])
        lz = float(self.config.find('box').attrib['lz'])
        for i in range(N):
            pos = [float(x) for x in t[i*3:i*3+3]]
            xyz[i] = numpy.array(pos)
        return xyz, (lx,ly,lz)

