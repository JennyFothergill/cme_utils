import numpy as np
import matplotlib.pyplot as plt
import argparse
import os


def main():
    parser = argparse.ArgumentParser(prog='logplot',
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--save',
                        action='store_true',
                        required=False,
                        help='''When given, save all figures
                        instead of showing them. Default behaviour
                        is to show plots instead of saving.''')
    parser.add_argument('-a', '--all',
                        action='store_true',
                        required=False,
                        help='''When given, skip the CLI queries and
                        instead make all plots. The first column is
                        always used for the x axis, and all subsequent
                        columns are plotted as y. Default behaviour
                        is to ask the user which plots to make.''')
    parser.add_argument('-i', '--index_skip',
                        type=int,
                        required=False,
                        default=0,
                        help='''Slice the data arrays so that the first
                        args.index_skip elements are omitted. Useful when
                        avoiding the large temp/energy peaks at the beginning
                        of a simulation.''')
    parser.add_argument('-x', '--x_val',
                        type=int,
                        required=False,
                        default=None,
                        help='''The integer column index from the logfile of the
                        x value to plot on the graph.''')
    parser.add_argument('-y', '--y_val',
                        type=int,
                        required=False,
                        default=None,
                        help='''The integer column index from the logfile of the
                        y value to plot on the graph.''')
    args, list_of_logs = parser.parse_known_args()

    start_index = args.index_skip  # Can use this later to skip things not in our window

    for i, log in enumerate(list_of_logs):
        plt.clf()
        log_data = np.genfromtxt(log)
        log_data_headers = np.genfromtxt(log, names=True).dtype.names
        title = os.path.splitext(log)[0]
        print("Plotting figures for", title)
        if (not args.all):
            if (args.x_val is None) and (args.y_val is None):
                if i == 0:
                    for i, x_options in enumerate(log_data_headers):
                        print(i, x_options)
                    x_vals = input("x val (num)? ")
                    x_vals = int(x_vals)
                    for i, y_options in enumerate(log_data_headers):
                        print(i, y_options)
                    y_vals = input("y val (num)? ")
                    y_vals = int(y_vals)
            else:
                x_vals = int(args.x_val)
                y_vals = int(args.y_val)
            y = log_data[:, y_vals]
            x = log_data[:, x_vals]
            x_label = log_data_headers[x_vals]
            y_label = log_data_headers[y_vals]
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            plt.plot(x[start_index:], y[start_index:], linestyle="None",
                     marker='o')
            if len(title) < 10:
                plt.title(title)
            if not args.save:
                plt.show()
            else:
                fig_title = "".join([title, "_", x_label, "_", y_label, ".pdf"])
                plt.savefig(fig_title)
                print("Figure saved as", fig_title)

        else:
            for y_index, y_option in enumerate(log_data_headers):
                if y_index == 0:
                    continue
                plt.clf()
                x = log_data[:, 0]
                y = log_data[:, y_index]
                x_label = log_data_headers[0]
                y_label = log_data_headers[y_index]
                plt.plot(x[start_index:], y[start_index:], linestyle="None",
                         marker='o')
                plt.xlabel(x_label)
                plt.ylabel(y_label)
                if len(title) < 10:
                    plt.title(title)
                if not args.save:
                    plt.show()
                else:
                    fig_title = "".join([title, "_", x_label, "_", y_label, ".pdf"])
                    plt.savefig(fig_title)
                    print("Figure saved as", fig_title)
