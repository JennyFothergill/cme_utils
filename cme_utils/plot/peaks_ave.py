import matplotlib.pyplot as plt
import numpy as np
import glob
import re 
import matplotlib.cm as cm
import sys

def make_plot(x, y, yerr):
    plt.errorbar(x, y, yerr=yerr, fmt='o')

def get_data(log_file):
    try:
        data = np.genfromtxt(log_file)
        data = np.atleast_2d(data)
        return data
    except (ValueError, IOError):
        print("Problem with this log file, skipping")
        print(log_file)
        print("\n")
        return None

if __name__ == "__main__":

    plot_folder="plots/"

    labels = []

    x_tmp = []
    y_tmp = []
    yerr_tmp = []
    list_of_directories = [vals for vals in sys.argv[1:]]


    num_plots = len(list_of_directories)
    #colors = iter(cm.inferno(np.linspace(0, 1, num_plots)))
    y_array = []
    y_std = []
    #print(list_of_directories)
    for directory in list_of_directories:
        try:
            x = float((re.search("T(\d\.\d)", directory).groups()[0]))
        except (AttributeError):
            x = float((re.search("T(\d\d\.\d)", directory).groups()[0]))
    #    color=next(colors)
        sub_dir = glob.glob(directory+'diffract/[0-9]')
        sub_dir += glob.glob(directory+'diffract/[0-9][0-9]')
        sub_dir += glob.glob(directory+'diffract/[0-9][0-9][0-9]')
        run_dir = directory#[:-9]
        window = np.genfromtxt(run_dir+"LJ_window.txt", delimiter=" ")
        SKIP = int(window[0]/10.0)
        #SKIP = -1
        y1_tmp =[]
        y0_tmp = []
        print(directory)
        #x = (re.search("\/([0-9]+)(?=[^\/]*$)", directory).groups()[0])
        for i, sd in enumerate(sub_dir):
            try:
                dir_num = re.search("\/([0-9]+)(?=[^\/]*$)", sd).groups()[0]
            except (AttributeError):
                break
            log_file = sd + "/peaks.txt"
            data = get_data(log_file)
            #print(SKIP,dir_num)
            if data !=None and int(dir_num) > SKIP:
                y1_tmp.append(data[0][1].flatten()) #First peak height
                y0_tmp.append(data[0][2].flatten()) #First peak location
                #print(data[0][1].flatten())
                #y = data[0][0].flatten()
                #plt.scatter(x,y)
    #            plt.scatter(x,y, color=color, label=label if i == 0 else "") #http://stackoverflow.com/questions/19385639/duplicate-items-in-legend-in-matplotlib/19385845#19385845
                #plt.errorbar(x,y,yerr=err)
                #make_plot(x, y, directory)
    #    plt.legend(ncol=5, loc='upper center',
    #           bbox_to_anchor=[0.5, 1.1],
    #           columnspacing=1.0, labelspacing=0.0,
    #           handletextpad=0.0, handlelength=1.5,
    #           fancybox=True)
    #        y_array.append(np.mean(y_tmp, dtype=np.float64))
    #        y_std.append(np.std(y_tmp, dtype=np.float64))
            y_a1 = np.mean(y1_tmp, dtype=np.float64)
            y_s1 = np.std(y1_tmp, dtype=np.float64)
            y_a0 = np.mean(y0_tmp, dtype=np.float64)
            y_s0 = np.std(y0_tmp, dtype=np.float64)
            #labels =
        #plt.subplot(211)
        #plt.errorbar(x, y_a1, yerr=y_s1, fmt='o', label=x)
        #plt.ylabel('First Peak Height')
        x_tmp.append(x)
        y_tmp.append(y_a1)
        yerr_tmp.append(y_s1)
        #plt.subplot(212)
        #plt.errorbar(x, y_a0, yerr=y_s0, fmt='o', label=x)
        #plt.ylabel('First Peak Location')
        #plt.xlabel('Temperature')
        #make_plot(x,y_a,y_s)
        #plt.show()
        #plt.savefig("plots/new-y1-" + label +".pdf")
        #plt.close()
    #plt.title('bdt-old-anneal-dt0.001-phi0.916-e0.5-P1.0-T*')
    #plt.show()
    print(x_tmp, y_tmp, yerr_tmp)
    #plt.savefig("plots/first_peak_location_height_bdt-old.pdf")
    #https://stackoverflow.com/questions/4805048/how-to-get-different-colored-lines-for-different-plots-in-a-single-figure
    with open("peak-data-rig-qnch.txt", "w") as text_file:
        text_file.write("#temp peak_height std\n")
        for _ in range(len(x_tmp)):
            text_file.write("{} {} {}\n".format(x_tmp[_],y_tmp[_],yerr_tmp[_]))
