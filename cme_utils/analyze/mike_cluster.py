

from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object
import mdtraj as md
import numpy as np 
import sys
import re


FRAME = -4


if __name__ == "__main__":


    DATA_DIR = "/Users/mikehenry/Projects/data/jan/paper-data/"
    #WORKING_DIR = sys.argv[1] # Include leading slash!!!
    WORKING_DIR_LIST = [vals for vals in sys.argv[1:]]


    t_vals = [] 
    clust_per = []

    for WORKING_DIR in WORKING_DIR_LIST:
        
        TRAJ_FILE = WORKING_DIR + "traj.dcd"
        TOP_FILE = WORKING_DIR + "restart.hoomdxml"

        t = md.load(TRAJ_FILE, top=TOP_FILE)
        DEBUG = False
        print(t)

        idx_stride = 48
        num_mon = 5
        n_chains = t.topology.n_residues  # 197
        axes = t.unitcell_lengths[-1]

        if DEBUG is True:
            n_atoms = 240*20
            idx_start = 0  # 240*10
        else:
            n_atoms = t.topology.n_atoms
            idx_start = 0



        def pbc_diff(p1, p2, axes):
            dr = p1 - p2
            for i, p in enumerate(dr):
                dr[i] = abs(dr[i])
                if dr[i] > axes[i]*0.5:
                    dr[i]-=axes[i]
            return np.linalg.norm(dr)


        class Chain(object):
            def __init__(self, xyz, idx, tag, traj):
                self.xyz = xyz
                self.idx = idx
                self.tag = tag
                self.traj = traj
                self.n_list = []
                self.clusterID = tag

            def n_check(self, chain1):
                count = 0
                TUNE = 2
                CUT = .16
                for i, point in enumerate(self.xyz):
                    for j, point1 in enumerate(chain1.xyz):
                        if pbc_diff(point1, point, axes) <= CUT:
                            count += 1
                if count >= TUNE:
                    self.n_list.append(chain1.tag)
                    if chain1.clusterID >= self.clusterID:
                        chain1.clusterID = self.clusterID
                    else:
                        self.clusterID = chain1.clusterID


        bb_index = [i for i in range(idx_start, n_atoms, idx_stride)]
        bb_xyz_raw = [t.xyz[-1, i, :] for i in bb_index]

        chain_list = []


        for i in range(0, t.topology.n_atoms, idx_stride*num_mon):  # Builds list of Chain

            idx = [i+0+(idx_stride*j) for j in range(num_mon)]
            xyz = [old_div((t.xyz[FRAME, i+0+(idx_stride*j), :] + t.xyz[FRAME, i+1+(idx_stride*j), :] +  t.xyz[FRAME, i+2+(idx_stride*j), :] +  t.xyz[FRAME, i+4+(idx_stride*j), :] +  t.xyz[FRAME, i+5+(idx_stride*j), :]+ t.xyz[FRAME, i+9+(idx_stride*j), :]),6.0) for j in range(num_mon)]
            chain_list.append(Chain(xyz, idx, old_div(i,240), t))

        change = False
        l_clust_old = sys.maxsize 
        while change is False:
            for i in range(len(chain_list)):
                for j in range(i+1, len(chain_list)):
                    chain_list[i].n_check(chain_list[j])
        
            clusters = []

            for chain in chain_list:
                clusters.append(chain.clusterID)

            clusters = list(set(clusters))
            l_clust_new = len(clusters)
            print(len(clusters))
            if l_clust_new < l_clust_old:
                change = False
                l_clust_old = l_clust_new
            else:
                change = True

        
        
        
        cluster_tags = []

        d = {}
        for chain in chain_list:
            for clust in clusters:
                if chain.clusterID == clust:
                    if clust in d:
                        d[clust].append(chain.tag)
                    else:
                        d[clust]=[chain.tag]
        count = 0
        c_chain = 0
        my_string = "mol representation VDW 1.000000 8.000000; "
        my_string += "mol material AOEdgy; "
        for k, dk in list(d.items()):
            if len(dk) > 1:
                my_string += "mol addrep 0; "
                my_string += "mol modcolor " + str(count) + " 0 ColorID " + str(count % 32)+"; "
                my_string += "mol modselect " + str(count) + " 0 not type CT and index "
                count += 1
                for x in dk:
                    my_string += str(x*240) + " to "+ str(240*x + 239) +" "
                    c_chain += 1
                my_string += "; "
        clust_per.append(old_div(c_chain,float(n_chains)))
        with open(WORKING_DIR+"NEW_cluster.tcl","w") as f:
            f.write(my_string)

        try:
            temp = ((re.search("T(\d\.\d)", WORKING_DIR)))
            t_vals.append(float(temp.groups()[0]))
            print(float(temp.groups()[0]))
        except (AttributeError):
            temp = ((re.search("T(\d\d\.\d)", WORKING_DIR)))
            t_vals.append(float(temp.groups()[0]))
            print(float(temp.groups()[0]))


    print(WORKING_DIR_LIST[0].split("/"))
    print(t_vals)
    print((t_vals,clust_per))
    with open(DATA_DIR + WORKING_DIR_LIST[0].split("/")[-2]+str(FRAME)+ ".txt","w") as f:
        f.write("#temp cluster% \n")
        for i, pairs in enumerate(t_vals):
            f.write(str(pairs) + " " + str(clust_per[i]) + "\n")
    #print len(clust_per)
    #print len(WORKING_DIR_LIST)
