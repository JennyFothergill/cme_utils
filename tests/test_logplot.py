import os
import pytest
import tempfile
import sys
from unittest.mock import patch

def test_logplot_finder():
    from cme_utils.plot.logplot import main
    with tempfile.NamedTemporaryFile() as tmp_file:
        with open(tmp_file.name, "w") as f:
            f.write("# x y \n")
            f.write("0 1\n")
            f.write("1 2\n")
        # Really ugly hack, need to refactor logplot so parser is in its own function
        test_args = ["foo", "-x", "0", "-y", "1", tmp_file.name]
        with patch.object(sys, 'argv', test_args):
            with patch('matplotlib.pyplot.show') as p:
                main()
